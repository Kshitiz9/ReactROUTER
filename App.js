
import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './Menu';
import About from './About';
import ContactUs from './ContactUs';
import Services from './Services';
import Blog from './Blog';

import {BrowserRouter, Route, Switch} from 'react-router-dom';

function App() {
  return (
    
         
         <BrowserRouter>   
         <div>

         

      
    
      <Menu/>
      <Switch>
      
      <Route path="/"  exact component ={Home} />
      <Route path="/about" component ={About} />
       <Route path="/services" component ={Services} />
       <Route path="/blog" component ={Blog} />
      <Route path="/contactus" component ={ContactUs} />
        
      </Switch>
      
      
    </div>
    </BrowserRouter>
    
  )
}
 const Home  =() =>{
           return(
               <div className ="contactusstyle">
               <h2> Welcome to my homepage. </h2>
               </div>
           )
       }

export default App;
